/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_grpcerror.h>

#include <gtest/gtest.h>

TEST(GrpcError, TestConstructor)
{
    const std::string errorMessage = "Something went wrong with foo()";
    grpc::Status grpcErrorStatus(grpc::StatusCode::ABORTED,
                                 "Operation aborted");

    const buildboxcommon::GrpcError e(errorMessage, grpcErrorStatus);
    ASSERT_EQ(e.status.error_code(), grpcErrorStatus.error_code());
    ASSERT_EQ(e.status.error_message(), grpcErrorStatus.error_message());
    ASSERT_STREQ(e.what(), errorMessage.c_str());
}
